# docker commands:
# docker system prune -a --volumes
# docker volume create builder_cache
# docker build . --tag mushkevych/embedded-builder --file embedded-builder.dockerfile
# docker -D run --name embedded-builder -it mushkevych/embedded-builder /bin/bash

# list of alpine packages: https://pkgs.alpinelinux.org/packages
FROM frolvlad/alpine-glibc:alpine-3.11_glibc-2.31

LABEL maintainer="mushkevych@gmail.com"
#LABEL embedded-builder.docker.version="0.1"

# OS-level required packages:
#   * dumb-init: a proper init system for containers, to reap zombie children
#   * linux-headers: commonly needed, and an unusual package name from Alpine.
#   * build-base: include basic development packages (gcc, g++, etc)
#   * bash gawk sed grep bc coreutils: bash & shell utils
#   * ca-certificates: for SSL verification during Pip and easy_install
ENV PACKAGES="\
  dumb-init \
  sudo \
  linux-headers \
  build-base \
  bash gawk wget sed grep bc curl coreutils \
  ca-certificates \
  gcc musl-dev \
  git scons bzip2-dev \
  help2man ffmpeg-libs texinfo cvs subversion \
"
RUN apk add --no-cache ${PACKAGES}

# python2 
RUN apk add --no-cache python2

# add python3, python3-dev and pip
# NOTE: do not remove ensurepip module
RUN apk add --no-cache python3 python3-dev && \
    python3 -m ensurepip && \
    pip3 install --no-cache --upgrade pip setuptools wheel 

# && \
#    if [[ ! -e /usr/bin/pip ]]; then ln -s pip3 /usr/bin/pip; fi && \
#    if [[ ! -e /usr/bin/python ]]; then ln -sf /usr/bin/python3 /usr/bin/python; fi

COPY entrypoint.alpine.sh /opt/embedded-builder/entrypoint.sh
RUN chmod +x /opt/embedded-builder/entrypoint.sh

WORKDIR /home/builder/embedded/
ENTRYPOINT ["/bin/sh","/opt/embedded-builder/entrypoint.sh"]
