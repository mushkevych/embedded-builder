#!/usr/bin/bash

cat <<SUDO >> /etc/sudoers
builder    ALL=(ALL) NOPASSWD: ALL
SUDO

# Set permissions correctly
# chown -R builder:builder /home/builder

# Drop in as the builder user
# "--" signify the end of command options, after which only positional parameters are accepted
exec runuser -u builder -- "$@"
