# docker commands:
# docker system prune -a --volumes
# docker volume create builder_cache
# docker build . --tag mushkevych/embedded-builder --file embedded-builder.dockerfile
# docker -D run --name embedded-builder -it mushkevych/embedded-builder /bin/bash


FROM ubuntu:18.04

RUN apt-get update
RUN apt-get install -y locales locales-all
ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

RUN \
  apt-get update && \
  apt-get install -y \
    apt-utils coreutils \
    build-essential \
    sudo \
    curl \
    wget \
    vim-tiny \
    gosu \
    openssh-client \
    pkg-config \
    unzip \
    git \
    subversion cvs help2man texinfo patch diffstat texi2html bzip2 tar gzip gawk \
    linux-headers-generic
    
    
# Python 2
#RUN \
#  apt-get update && \
#  apt-get install -y python python-pip python-dev
    
# Python 3
RUN \
  apt-get update && \
  apt-get install -y python3.7 python3-pip python3.7-dev
    
    
# make /bin/sh symlink to bash instead of dash:
RUN echo "dash dash/sh boolean false" | debconf-set-selections
RUN DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash


COPY entrypoint.sh /opt/embedded-builder/entrypoint.sh
RUN chmod +x /opt/embedded-builder/entrypoint.sh

RUN groupadd builder && \
    adduser --disabled-password --gecos "builder user" --home /home/builder --shell /bin/bash builder --ingroup builder    

# format username:new_password
RUN echo "builder:builder" | chpasswd 2>/dev/null
    
WORKDIR /home/builder/embedded
ENTRYPOINT ["/bin/sh","/opt/embedded-builder/entrypoint.sh"]
