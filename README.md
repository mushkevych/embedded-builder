# Embedded Builder Container

This Docker container exists for the purpose of:
- building embedded Linux kernel
- building embedded packages
- running tests


## Build the image

```
docker volume create builder_cache
docker build . --tag mushkevych/embedded-builder --file embedded-builder.dockerfile
```


## Usage

You can start this container from your current working directory with a command:

```
cat <<EOF >> ~/.bashrc
buildshell ()
{
    docker run --rm -ti --privileged -v \`pwd\`:/home/builder/embedded \
      --mount source=builder_cache,target=/home/builder/cache \
      mushkevych/embedded-builder ${@:-/bin/bash}
}
EOF

```

**NOTE**: After adding above to the `.bashrc` you can either login/logout or run `bash -l` to "reload" the shell.
