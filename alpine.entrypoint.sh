#!/usr/bin/bash

addgroup builder && adduser -D --home /home/builder --shell /bin/bash builder -G builder

# format username:new_password
echo "builder:builder" | chpasswd 2>/dev/null

cat <<ALIAS >> /home/builder/.bashrc
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
alias grep='grep --color=auto'
alias l='ls -CF'
alias la='ls -A'
alias ll='ls -alF'
alias ls='ls --color=auto'
ALIAS

cat <<SUDO >> /etc/sudoers
builder    ALL=(ALL) NOPASSWD: ALL
SUDO

# Set permissions correctly
# chown -R builder:builder /home/builder

# Drop in as the builder user
exec su -l builder -c 'source /home/builder/.bashrc' -c "$@"
# exec "$@"
